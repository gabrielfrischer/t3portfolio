import { createRouter } from "./context";
import { z } from "zod";
import recaptcha from "../../pages/api/recaptcha";
import axios from "axios";

export const recaptchaRouter = createRouter()
    .query("recaptcha", {
    input: z
      .object({
        captcha_value: z.string().nullish(),
      })
      .nullish(),
    async resolve({ input }) {
      const recaptcha_response = await axios.post(
        'https://www.google.com/recaptcha/api/siteverify',
        {
                        'secret': process.env.RECAPTCHA_SECRET_KEY,
            'response': input?.captcha_value,
        }

    
  ) 
     console.log('Server recaptcha response: ', recaptcha_response)

      return recaptcha_response.data;
    },
  })

