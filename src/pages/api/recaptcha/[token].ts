
import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";


 const recaptcha = async (req: NextApiRequest, res:NextApiResponse) => {

      console.log('req.query.token: ', req.query.token)
      console.log('process.env.RECAPTCHA_SECRET_KEY: ', process.env.RECAPTCHA_SECRET )
        const recaptcha_secret = process.env.RECAPTCHA_SECRET
      try {
  const recaptcha_response = await axios.post(
        'https://www.google.com/recaptcha/api/siteverify', undefined, {
        params: {
            'secret': recaptcha_secret,
            'response': req.query.token,
        } }

    
  )  
  console.log('Recaptcha_response: ', recaptcha_response.data)
  console.log('Recaptcha response state', recaptcha_response.status)
  res.status(200).json(recaptcha_response.data.success);
        }
        catch(e){
            console.log(e)
              res.status(200).json(e)
        }



}
export default recaptcha