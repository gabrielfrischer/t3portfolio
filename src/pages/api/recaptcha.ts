
// src/pages/api/examples.ts
import type { NextApiRequest, NextApiResponse } from "next";
import { prisma } from "../../server/db/client";
import axios from 'axios';

const recaptcha = async (req: NextApiRequest, res: NextApiResponse) => {
  console.log('Recaptcha request: ', req)
  const recaptcha_response = await axios.post(
        'https://www.google.com/recaptcha/api/siteverify',
        {
                        'secret': process.env.RECAPTCHA_SECRET_KEY,
            'response': req,
        }

    
  ) 
  res.status(200).json(recaptcha_response);
};

export default recaptcha;
