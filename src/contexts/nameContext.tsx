import { createContext } from "react";

export interface NameInterface {
  name: string | null;
}

export const NameContext = createContext<NameInterface | null>(null);
