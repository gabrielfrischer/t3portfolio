import { IconType } from "react-icons";
import {
  FiHome,
  FiTrendingUp,
  FiCompass,
  FiStar,
  FiMessageCircle,
} from "react-icons/fi";

interface LinkItemProps {
  name: string;
  icon: IconType;
}
const LinkItems: Array<LinkItemProps> = [
  { name: "Home", icon: FiHome },
  { name: "About Me", icon: FiTrendingUp },
  { name: "My Stack", icon: FiCompass },
  { name: "Portfolio", icon: FiStar },
  { name: "Contact", icon: FiMessageCircle },
];

export default LinkItems;
