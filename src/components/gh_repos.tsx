import { useEffect, useState } from "react";
import {
  Box,
  chakra,
  Heading,
  Text,
  SimpleGrid,
  Stat,
  StatLabel,
  StatNumber,
  Container,
  Stack,
  HStack,
  IconButton,
  useColorModeValue,
  Link,
  VStack,
} from "@chakra-ui/react";
import { Repos } from "./about_me";
import { FaGithub } from "react-icons/fa";

function Feature(repos: Repos) {
  const { full_name, created_at, url } = repos;
  return (
    <Box
      p={5}
      shadow="md"
      borderWidth="1px"
      bg={useColorModeValue("white", "gray.700")}
      m={2}
      style={{
        flex: "0 0 auto",
      }}
    >
      <HStack>
        <VStack className="flex justify-between ">
          <Heading className="break-words" fontSize="xl">
            {full_name}
          </Heading>
          <Text className="align-bottom text-right w-full">{created_at}</Text>
        </VStack>{" "}
        <IconButton
          fontSize="20px"
          icon={<FaGithub />}
          aria-label="go to github repo"
          as={Link}
          href={url}
          isExternal
        />{" "}
      </HStack>
    </Box>
  );
}

interface Props {
  repos: Repos[];
}
export default function GH_Repos({ repos }: Props) {
  console.log("repos2: ", repos);
  return (
    <Box className="flex overflow-x-auto  w-5/6 lg:w-4/6 my-5">
      {repos.length > 0 &&
        repos.map((r) => {
          return (
            <Feature
              key={r.key}
              full_name={r.full_name}
              created_at={r.created_at}
              url={r.url}
            />
          );
        })}
    </Box>
  );
}
