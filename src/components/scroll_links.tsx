import {
  Link as ChakraLink,
  Flex,
  useColorModeValue,
  FlexProps,
  Icon,
  Button,
} from "@chakra-ui/react";
import { IconType } from "react-icons";
import LinkItems from "./linkItems";

interface NavItemProps extends FlexProps {
  icon: IconType;
  children: string;
}
const NavLink = ({ icon, children }: NavItemProps) => {
  function handleClick(f_name: string) {
    console.log("Clicked");
    console.log("fname: ", f_name);
    const section = document.querySelector("#" + f_name);
    console.log("section outside if:  ", section);
    if (section) {
      console.log("section exists: ", section);
      section.scrollIntoView({
        behavior: "smooth",
      });
    }
  }

  const f_name = children.toLowerCase().replace(/\s+/g, "");

  return (
    <ChakraLink
      onClick={() => handleClick(f_name)}
      style={{ textDecoration: "none" }}
    >
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: "cyan.400",
          color: "white",
        }}
      >
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: "white",
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </ChakraLink>
  );
};

const NavLinks = () => {
  return (
    <>
      {LinkItems.map((link) => (
        <NavLink key={link.name} icon={link.icon}>
          {link.name}
        </NavLink>
      ))}
    </>
  );
};

export default NavLinks;
