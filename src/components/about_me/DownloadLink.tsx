import { Link as ChakraLink } from "@chakra-ui/react";
import Link from "next/link";
interface Props {
  path: string;
  text: string;
}

const DownloadLink = (props: Props) => {
  const { path, text } = props;

  return (
    <Link download href={path} passHref>
      <ChakraLink>{text}</ChakraLink>
    </Link>
  );
};

export default DownloadLink;
