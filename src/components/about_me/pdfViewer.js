import { useState } from "react";
// import default react-pdf entry
import { Document, Page, pdfjs } from 'react-pdf/dist/esm/entry.webpack5';
// import pdf worker as a url, see `next.config.js` and `pdf-worker.js`




export default function PDFViewer() {
  const [file, setFile] = useState("/WebViewResume.pdf");
  const [numPages, setNumPages] = useState(null);



  function onDocumentLoadSuccess({ numPages: nextNumPages }) {
    setNumPages(nextNumPages);
  }

  return (
    <div>

      <div >
        <Document 
       className={"PDFDocument p-5"} 
      file={file} onLoadSuccess={onDocumentLoadSuccess}>
          {Array.from({ length: numPages }, (_, index) => (
            <Page
            className={"PDFPage PDFPageOne"}
              key={`page_${index + 1}`}
              pageNumber={index + 1}
              renderAnnotationLayer={false}
              renderTextLayer={false}
            />
          ))}
                      
        </Document>   
      </div>
    </div>
  );
}
