import { motion, useInView } from "framer-motion";
import React, { useRef } from "react";
import TechIcon from "./techIcon";
import {
  SiJavascript,
  SiTypescript,
  SiTailwindcss,
  SiChakraui,
  SiDjango,
  SiPostgresql,
  SiNextdotjs,
  SiHeroku,
  SiVercel,
  SiPrisma,
  SiMaterialdesign as SiMaterialui,
} from "react-icons/si";
import { DiReact, DiVim } from "react-icons/di";
import {
  FaNodeJs,
  FaNpm,
  FaAws,
  FaPython,
  FaDocker,
  FaGithub,
  FaGitlab,
  FaLinux,
  FaYarn,
  FaHtml5,
  FaCss3Alt,
  FaBootstrap,
} from "react-icons/fa";

import { AiFillInteraction } from "react-icons/ai";
import { Box, Wrap, Text } from "@chakra-ui/react";

interface Props {}

const container = {
  hidden: { opacity: 0 },
  show: {
    delay: 1.5,
    opacity: 1,
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const vItem = {
  hidden: { opacity: 0, y: 50 },
  show: {
    opacity: 1,
    y: 0,
    transition: {
      ease: "linear",
    },
  },
};

const TechIcons = (props: Props) => {
  const techIconsRef = useRef(null);
  const techInView = useInView(techIconsRef, { once: true, amount: 0.2 });

  return (
    <motion.div
      ref={techIconsRef}
      variants={container}
      animate={techInView ? "show" : "hidden"}
      className="grid grid-cols-9"
    >
      <Box className="col-span-9  ">
        <Wrap className="w-auto items-center relative m-auto p-8">
          <TechIcon icon={<SiJavascript />} text="JavaScript" />
          <TechIcon icon={<SiTypescript />} text="TypeScript" />

          <TechIcon icon={<FaNodeJs />} text="nodejs" />
          <TechIcon icon={<DiReact />} text="react" />
          <TechIcon icon={<SiNextdotjs />} text="nextjs" />

          <TechIcon icon={<FaNpm />} text="npm" />
          <TechIcon icon={<FaYarn />} text="yarn" />

          <TechIcon icon={<FaAws />} text="aws" />
          <TechIcon icon={<SiVercel />} text="vercel" />
          <TechIcon icon={<SiHeroku />} text="heroku" />
          <TechIcon icon={<FaPython />} text="python" />

          <TechIcon icon={<SiDjango />} text="django" />
          <TechIcon icon={<SiPostgresql />} text="postgres" />

          <TechIcon icon={<SiPrisma />} text="prisma" />
          <TechIcon icon={<FaDocker />} text="docker" />
          <TechIcon icon={<FaGithub />} text="github" />
          <TechIcon icon={<FaGitlab />} text="gitlab" />
          <TechIcon icon={<FaLinux />} text="linux" />

          <TechIcon icon={<FaHtml5 />} text="html5" />
          <TechIcon icon={<FaCss3Alt />} text="css3" />

          <TechIcon icon={<SiChakraui />} text="chakra-ui" />
          <TechIcon icon={<SiTailwindcss />} text="tailwind" />
          <TechIcon icon={<SiMaterialui />} text="MUI" />
          <TechIcon icon={<FaBootstrap />} text="bootstrap" />

          <TechIcon icon={<DiVim />} text="vim" />
        </Wrap>
      </Box>
      <Box className="col-span-9 px-8 lg:col-span-6 lg:col-start-3 mt-4">
        <ul className="w-full ">
          <motion.li variants={vItem}>
            <Text className="interFont text-2xl font-medium text-left">
              ⚡ Develop highly interactive & responsive UI&apos;s
            </Text>
          </motion.li>
          <motion.li variants={vItem}>
            <Text className="interFont text-2xl font-medium text-left">
              ⚡ Test and deploy for production on platforms such as
              AWS/Vercel/Heroku
            </Text>
          </motion.li>
          <motion.li variants={vItem}>
            <Text className="interFont text-2xl font-medium text-left">
              ⚡ Implement Progressive Web Applications {"( PWA's)"} in normal
              and SPA stacks
            </Text>
          </motion.li>
        </ul>
      </Box>
    </motion.div>
  );
};

export default TechIcons;
