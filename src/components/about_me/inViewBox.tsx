import { Center } from "@chakra-ui/react";
import { useInView } from "framer-motion";
import { ReactNode, useRef } from "react";
import ChakraBox from "../chakraBox";

interface InViewBoxProps {
  children: string | ReactNode;
  className?: string;
  delay?: number;
  bgGradient?: string;
  fromL?: boolean;
  c_h?: string;
  id?: string;
}

function InViewBox(props: InViewBoxProps) {
  const {
    children,
    className,
    c_h,
    bgGradient,
    fromL,
    id,
    delay = 0.5,
  } = props;
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true, amount: 0.2 });

  return (
    <ChakraBox
      initial={{
        x: 200,
        opacity: 0,
      }}
      animate={{
        x: isInView ? 0 : fromL ? -200 : 200,
        opacity: isInView ? 1 : 0,
        transition: {
          duration: 0.9,
          ease: [0.17, 0.55, 0.55, 1],
          delay: delay,
        },
      }}
      className={className}
      bgGradient={bgGradient}
      ref={ref}
    >
      <Center id={id} className={c_h ? c_h : "h-full"}>
        {children}
      </Center>
    </ChakraBox>
  );
}
export default InViewBox;
