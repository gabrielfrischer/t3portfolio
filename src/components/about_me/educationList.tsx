import { ExternalLinkIcon, LinkIcon } from "@chakra-ui/icons";
import {
  Box,
  VStack,
  Button,
  Flex,
  Divider,
  chakra,
  Grid,
  GridItem,
  Container,
  Link,
  HStack,
  IconButton,
} from "@chakra-ui/react";
import { motion } from "framer-motion";
import Image from "next/image";

interface FeatureProps {
  heading: string;
  text: string;
  link: string;
}

const EducationItems = {
  title: "",
};

const Feature = ({ heading, text, link }: FeatureProps) => {
  return (
    <GridItem>
      <Link href={link} fontSize="xl" isExternal>
        {heading}
        <ExternalLinkIcon mx="2px" />
      </Link>
      <chakra.p>{text}</chakra.p>{" "}
    </GridItem>
  );
};

export default function EducationList() {
  return (
    <Box as={Container} maxW="7xl" mt={14} p={4}>
      <Grid
        templateColumns={{
          base: "repeat(1, 1fr)",
          sm: "repeat(2, 1fr)",
          md: "repeat(1, 1fr)",
        }}
        gap={4}
      >
        <GridItem colSpan={1}>
          <Image
            src="/UCSD_Seal.png"
            width={100}
            height={100}
            alt="ucsd seal"
          />
        </GridItem>
        <GridItem>
          <Flex>
            <VStack
              alignItems="flex-start"
              className="interFont"
              spacing="20px"
            >
              <chakra.h2 fontSize="3xl" fontWeight="700">
                University of California, San Diego
              </chakra.h2>
              <chakra.h3 fontSize="3xl">2014-2018</chakra.h3>
              <chakra.p fontSize="xl">
                Graduated with B.S in Human Biology with specialty in
                Neuroscience
              </chakra.p>
            </VStack>
          </Flex>
        </GridItem>
      </Grid>
      <Divider mt={12} mb={12} />
      <motion.h1 className="interFont text-center text-6xl  leading-normal font-bold  text-slate-400">
        Resume
      </motion.h1>
      <Grid
        templateColumns={{
          base: "repeat(1, 1fr)",
          sm: "repeat(2, 1fr)",
          md: "repeat(4, 1fr)",
        }}
        gap={{ base: "8", sm: "12", md: "16" }}
      >
        <Feature
          heading={"Real Estate Agent"}
          text={"Current CTO at Angels Realty"}
          link={
            "https://www2.dre.ca.gov/PublicASP/pplinfo.asp?License_id=02174129"
          }
        />
        <Feature
          heading={"Python Certified"}
          text={"From Hackernoon"}
          link={"https://www.hackerrank.com/certificates/6dfd4674b497"}
        />
        <Feature
          heading={"Got in the News"}
          text={"From a design competition at UCSD"}
          link={
            "https://www.sandiegouniontribune.com/sdut-design-competition-ucsd-engineering-2016jun04-story.html"
          }
        />
        <Feature
          heading={"Started a Startup"}
          text={
            "Called Embrace Healthwear to prevent falling injuries in older adults"
          }
          link={"https://angel.co/company/embrace-healthwear/jobs"}
        />
      </Grid>
    </Box>
  );
}
