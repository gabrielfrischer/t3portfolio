import {
    useColorMode,
  ChakraProvider,
  extendTheme,
  Container,
  Heading,
  Button,
  VStack,
  HStack,
  Text,
  Box,
  Flex,
  Tag,
  Wrap,
  WrapItem
} from "@chakra-ui/react";

import Image from "next/image";


import { v4 as uuidv4 } from "uuid";

import ChakraCarousel from "./chakraCarousel";




const projects = [
  {
    heading: "The Spiritual Iron Dome, TSI\u2122",
    description: "A grassroots movement to protect IDF soldiers and the world from terror groups",
    path: "/TSI.gif",
    url: "https://spiritualirondome.com",
    languages: ["NextJS 14", "Prisma", "ShadCN UI", "Tailwind", "Framer Motion"],
    host: "Vercel",
    uuid:uuidv4()
  },
  {
    heading: "Portfolio",
    description: "My Online Portfolio, sooooo meta",
    path: "/portfolio.gif",
    url: "https://gabrielfrischer.github.io/resume",
    languages: ["NextJS", "Prisma", "TRPC", "ChakraUI", "TailwindCSS"],
    host: " Vercel",
    uuid:uuidv4()
  },

  {
    heading: "Angels Realty",
    description:
      "Web app and automatic scraper for full service brokerage in Socal",
    path: "/angelsrealty.gif",
    url: "https://www.angelsrealty.net",
    languages: ["React", "Python", "Django", "MUI", "Celery"],
    host: "Heroku",
    uuid:uuidv4()
  },
  {
    heading: "Texas Licensing",
    description:
      "Web app which licenses contractors through testing and certification, and allows for public verification of licensees.",
    path: "/tlb.gif",
    url: "https://texaslicensing.herokuapp.com",
    languages: ["React", "Python", "Django", "Bootstrap"],
    host: " Heroku",
    uuid:uuidv4()
  },
  {
    heading: "Power Washing Pros",
    description: "Web app CRM for power washing company",
    path: "/pwp.gif",
    url: "https://powerwashingpros.herokuapp.com",

    languages: ["React", "Python", "Django", "Bootstrap", "Celery"],
    host: " Heroku",
    uuid:uuidv4()
  },
];


function UseChakraCarousel() {
  
  const { colorMode, toggleColorMode } = useColorMode();

  return (

      <ChakraCarousel gap={32}>
        {projects.map((post, index) => (
          <Flex
            key={index}
            boxShadow="rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px"
            justifyContent="space-between"
            flexDirection="column"
            overflow="hidden"
            className="interFont"
            color={colorMode == "dark" ? "gray.50" : "gray.700"}
            bg={colorMode == "dark" ? "gray.700" : "gray.50"}
            rounded={5}
            flex={1}
          >
            <div
              className="h-[200px] lg:h-[250px]"
              style={{
                width: "100%",
                position: "relative",
              }}
            >
              <Image src={post.path} layout="fill" alt="angelsrealty" />
            </div>

            <Box p={5}>
              <VStack mb={6}>
                <Heading
                  fontSize={{ base: "xl", md: "2xl" }}
                  textAlign="center"
                  w="full"
                  className="font-light"
                  mb={2}
                >
                  {post.heading}
                </Heading>
                <Text w="full">{post.description}</Text>
              </VStack>
              <Wrap justifyContent="space-between">
                    
                    <WrapItem>
                  <Tag  size="sm"  colorScheme="green">
                    Stack
                  </Tag>
                  </WrapItem>
                  {post.languages.map((l) => {
                    return (
                        <WrapItem key={uuidv4()}>
                          <Tag  size="sm"  variant="outline" colorScheme="green">
                   {l}
                  </Tag>
                    </WrapItem>
                    )
                  })}
                  <WrapItem>
                  <Tag size="sm" colorScheme="cyan">
                    {"Host"}
                  </Tag>
                    </WrapItem>

                  <WrapItem>
                  <Tag size="sm" variant="outline" colorScheme="cyan">
                    {post.host}
                  </Tag>
                        </WrapItem>
              </Wrap>
              <a target="_blank" rel="noreferrer" href={post.url}>
                <Button
                  w={"100%"}
                  mt={3}
                  colorScheme="cyan"
                  fontWeight="bold"
                  color="white"
                  size="sm"
                >
                  View
                </Button>
              </a>
            </Box>
          </Flex>
        ))}
      </ChakraCarousel> 
  );
}

export default UseChakraCarousel
