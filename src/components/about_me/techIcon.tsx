import {
  useColorMode,
  useColorModeValue,
  VStack,
  WrapItem,
} from "@chakra-ui/react";
import { motion } from "framer-motion";
import {
  cloneElement,
  JSXElementConstructor,
  ReactElement,
  useEffect,
  useState,
} from "react";

interface TechIconProps {
  text: string;
  icon: ReactElement<any, string | JSXElementConstructor<any>>;
  color?: string;
}

const item = {
  hidden: { opacity: 0, x: 700 },
  show: {
    opacity: 1,
    x: 0,
    transition: {
      ease: "linear",
    },
  },
};

function TechIcon(props: TechIconProps) {
  const { icon, text } = props;

  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <WrapItem>
      <motion.span
        variants={item}
        whileHover={{
          scale: 1.2,
          transition: {
            duration: 0.05,
          },
        }}
        whileTap={{
          scale: 1.2,
          transition: {
            duration: 0.05,
          },
        }}
        style={{
          width: "50px",

          color: colorMode == "dark" ? "blue.200" : "gray.700",
        }}
        aria-label={text}
        className="duration-500 motion-safe:hover:scale-105"
      >
        <VStack>
          {cloneElement(icon, {
            style: {
              fontSize: "50px",
              marginTop: "10px",
            },
          })}
          <motion.span
            style={{
              fontSize: "12px",
            }}
          >
            {text}
          </motion.span>
        </VStack>
      </motion.span>
    </WrapItem>
  );
}

export default TechIcon;
