import { useState, useEffect } from "react";

import React from "react";
import { motion, useAnimationControls, Variants } from "framer-motion";
import { ColorMode } from "@chakra-ui/react";

interface Props {
  colorMode: ColorMode;
}

const Stars = (props: Props) => {
  const { colorMode } = props;
  const starCtrl = useAnimationControls();
  const [starNumber, setStarNumber] = useState(100);
  const [width, setWidth] = useState("100");
  const [height, setHeight] = useState("100");
  const [x_skip_length, setXSkipLength] = useState(1);
  const [steps, setSteps] = useState(parseInt(width) / x_skip_length);
  const [points, setPoints] = useState<number[][]>([]);
  const starVariants: Variants = {
    initial: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 6,
      },
    },
  };

  const randomRadiusAmount = (range: number) => {
    const val = Math.random() * range;
    return val;
  };

  const randomBlinkDuration = (range: number) => {
    const val = Math.random() * range;
    if (val > 1) return val;
    else return 0.2;
  };
  const randomNumInRange = (range: number) => {
    const plusOrMinus = Math.random() < 0.5 ? -1 : 1;
    const val = plusOrMinus * Math.floor(Math.random() * range);
    return val;
  };

  const create_points = () => {
    const points = [];
    console.log("creating points");
    for (let i = 0; i < starNumber; i++) {
      console.log("pushing star point");
      points.push([i, randomNumInRange(30) + 20]);
    }
    return points;
  };

  const showStars = () => {
    if (colorMode === "light") {
      console.log("Hiding stars");
      starCtrl.start("initial");
    } else {
      console.log("Showing Stars");
      starCtrl.start("visible");
    }
  };

  useEffect(() => {
    setPoints(create_points());
  }, []);

  useEffect(() => {
    showStars();
  }, [colorMode]);

  return (
    <motion.svg
      style={{
        position: "absolute",
        zIndex: 995,
        pointerEvents: "none",
      }}
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid slice"
      width="100%"
      height="75%"
      variants={starVariants}
      initial="initial"
      animate={starCtrl}
    >
      {points.map((c, idx) => (
        <motion.circle
          key={idx}
          cx={c[0]}
          cy={c[1]}
          r={randomRadiusAmount(0.2)}
          fill="white"
          initial={{ fillOpacity: 0 }}
          animate={{
            fillOpacity: 1,
            transition: {
              delay: randomBlinkDuration(20),
              duration: 1.5,
              repeat: Infinity,
              repeatType: "mirror",
            },
          }}
        />
      ))}
    </motion.svg>
  );
};

export default Stars;
