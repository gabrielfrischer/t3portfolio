import { useMediaQuery } from "@chakra-ui/react";
import { motion, MotionStyle, Variants } from "framer-motion";
import React from "react";

interface Props {
  style?: MotionStyle;
  transformY?: number;
  transformX?: number;
  offset: number;
}

function Water(props: Props) {
  const { offset, style, transformX, transformY } = props;

  const [isLargerThan1280] = useMediaQuery("(min-width: 480px)");
  const waterVariants: Variants = {
    initial: {
      pathLength: 0,
      fillOpacity: 0,
      d: "M 30 0 L 28 0 C 26 6 15 14 20 23 S 24 41 7 52 S 17 63 15 71 S 3 90 10 100 M 30 0 C 33 11 22 19 32 30 S 10 56 24 59 S 26 69 38 78 S 77 95.6667 79 100 L 10 100",
    },
    wave: {
      d: "M 30 0 L 28 0 C 29 5 32 11 19 21 S 35 40 9 45 S 24 59 12 71 S 16 78 10 100 M 30 0 C 36 14 15 28 31 35 S -1 49 31 58 S 13 76 37 79 S 77 95.6667 79 100 L 10 100",
      pathLength: 1,
      fillOpacity: 1,
      transition: {
        fillOpacity: {
          delay: 5,
          duration: 1.4,
        },
        pathLength: {
          delay: 1,
          duration: 10,
          type: "tween",
        },
        d: {
          delay: 2,
          duration: 5.5,

          repeat: Infinity,
          repeatType: "mirror",
          ease: "easeInOut",
        },
      },
    },
  };

  const waterContainerVariants: Variants = {
    initial: {
      viewBox: "0 200 60 700",
    },
    flow: {
      viewBox: "0 0 60 98",

      transition: {
        delay: 0,
        duration: 5,
      },
    },
  };

  return (
    <motion.svg
      width="100%"
      stroke="#fff"
      height={isLargerThan1280 ? "42%" : "42%"}
      viewBox="0 200 60 95"
      preserveAspectRatio="xMidYMax meet"
      variants={waterContainerVariants}
      initial="initial"
      animate="flow"
      style={{
        position: "absolute",
        bottom: "0px",
        margin: "0 auto",
        overflowY: "hidden",
        zIndex: 992,
        pointerEvents: "none",
        ...style,
      }}
    >
      <motion.g initial="initialDown" animate="animateUp">
        <motion.path
          fill="url(#gradientwater)"
          scale={2}
          opacity={0.9}
          d="M 30 0 L 28 0 C 26 6 15 14 20 23 S 24 41 7 52 S 17 63 15 71 S 3 90 10 100 M 30 0 C 33 11 22 19 32 30 S 10 56 24 59 S 26 69 38 78 S 77 95.6667 79 100 L 10 100"
          variants={waterVariants}
          initial="initial"
          animate="wave"
        />
      </motion.g>
      <defs>
        <motion.radialGradient
          id="gradientwater"
          fr={0.2}
          fx={0.32}
          fy={0.32}
          r={0.7}
          animate={{ fr: 0.01, fx: 0.2, fy: 0.35, r: 0.6 }}
          transition={{
            repeat: Infinity,
            repeatType: "mirror",
            ease: "easeInOut",
            duration: 3,
          }}
        >
          <stop offset="0%" stopColor="#99fff3" />
          <stop offset="100%" stopColor="#00b2ff" />
        </motion.radialGradient>
      </defs>
    </motion.svg>
  );
}

export default Water;
