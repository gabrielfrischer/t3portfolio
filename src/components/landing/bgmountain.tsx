import { useEffect, useState } from "react";
import {
  motion,
  MotionStyle,
  Variants,
  useAnimationControls,
  useScroll,
  MotionValue,
} from "framer-motion";
import { ColorMode } from "@chakra-ui/react";

const rootStyle: MotionStyle = {
  position: "absolute",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  // transform: "translateZ(-1px) scale(2)",
};

interface Props {
  transformY?: number;
  transformX?: number;
  style?: MotionStyle;
  scrollY: number;
  offset: number;
  colorMode: ColorMode;
  delay: number;
  reverse?: boolean;
  rotateX: MotionValue<number>;
}

const defaultProps: Partial<Props> = {
  transformY: 0,
};

export default function BgMountain(props: Props) {
  const spreadProps = { ...defaultProps, ...props };
  const mountainControl = useAnimationControls();
  const fillControl1 = useAnimationControls();
  const fillControl2 = useAnimationControls();
  const [width, setWidth] = useState("100");
  const [height, setHeight] = useState("100");
  const [svgPath, setSvgPath] = useState<string | undefined>("");

  const {
    scrollY,
    offset,
    transformY,
    transformX,
    style,
    colorMode,
    delay,
    reverse,
    rotateX,
  } = spreadProps;
  const [x_skip_length, setXSkipLength] = useState(4);
  const [steps, setSteps] = useState(parseInt(width) / x_skip_length);

  const mountainVariants: Variants = {
    initialDown: {
      y: 200,
      pathLength: 0,
    },
    animateUp: {
      y: transformY,

      transition: {
        duration: 3,
        delay: delay,
      },
    },
    animatePath: {
      pathLength: 1,
      transition: {
        duration: 3.5,
      },
    },
  };

  const fillAnimation1: Variants = {
    day: {
      stopColor: "#FF9205",
      transition: {
        duration: 3,
      },
    },
    night: {
      stopColor: "#003daa",
      transition: {
        duration: 3,
      },
    },
  };

  const fillAnimation2: Variants = {
    day: {
      stopColor: "#C6735F",
      transition: {
        duration: 3,
      },
    },
    night: {
      stopColor: "#000",
      transition: {
        duration: 3,
      },
    },
  };

  const mountainRiseThenPathDrawSequence = async () => {
    await mountainControl.start("animateUp");
    return await mountainControl.start("animatePath");
  };

  const randomNumInRange = (range: number) => {
    const plusOrMinus = Math.random() < 0.5 ? -1 : 1;
    const val = plusOrMinus * Math.floor(Math.random() * range);
    return val;
  };

  const create_points = () => {
    //set range from 0 - 100 relative to viewBox

    const points = [];
    if (reverse) {
      for (let i = 0; i < steps + 1; i++) {
        //for each steps, push new [x,y] coordinate to points array
        points.push([100 - i * x_skip_length, randomNumInRange(5) + 60]);
      }
    } else {
      for (let i = 0; i < steps + 1; i++) {
        //for each steps, push new [x,y] coordinate to points array
        points.push([i * x_skip_length, randomNumInRange(5) + 60]);
      }
    }

    return points;
  };

  // convert points into SVG path
  const getPath = () => {
    const points = create_points();
    // add first M (move) command
    let path = `M ${reverse ? 150 : -50} ${height}`;

    // iterate through points adding L (line) commands to path
    points.forEach((val, idx) => {
      if (idx < 1) {
        path += ` L ${val[0]} ${val[1]} `;
      } else if (idx > steps) {
        path += ` M ${val[0]} ${val[1]}`;
      } else {
        path += ` L ${val[0]} ${val[1]}`;
      }
    });

    // close path
    path += ` L ${
      reverse ? 100 - parseInt(width) * 1.1 : parseInt(width) * 1.1
    } ${height} M 0 ${height}   Z`;

    return path;
  };

  const changeMountainColor = () => {
    if (colorMode === "dark") {
      fillControl1.start("night");
      fillControl2.start("night");
    } else {
      fillControl1.start("day");
      fillControl2.start("day");
    }
  };

  useEffect(() => {
    changeMountainColor();
  }, [colorMode]);

  useEffect(() => {
    //generate paths for both bg mountains 1 and 2
    const path = getPath();
    setSvgPath(path);

    mountainRiseThenPathDrawSequence();
  }, []);

  return (
    <motion.svg
      width="100%"
      height="60%"
      preserveAspectRatio="none"
      viewBox={`0 0 ${width} ${height}`}
      style={{
        pointerEvents: "none",
        position: "absolute",
        right: transformX,
        ...style,
        ...rootStyle,
      }}
      animate={{
        y: `${offset * (scrollY * 0.2)}px`,
      }}
    >
      <defs>
        <motion.radialGradient
          id="bgMountainGradient1"
          fr={0.2}
          fx={0.32}
          fy={0.32}
          r={0.7}
          animate={{ fr: 0.05, fx: 0.2, fy: 0.35, r: 0.6 }}
          transition={{
            repeat: Infinity,
            repeatType: "mirror",
            ease: "easeInOut",
            duration: 3,
          }}
        >
          <motion.stop
            offset="0%"
            variants={fillAnimation1}
            initial="day"
            animate={fillControl1}
          />
          <motion.stop
            offset="100%"
            variants={fillAnimation2}
            initial="day"
            animate={fillControl2}
          />
        </motion.radialGradient>
      </defs>
      <motion.path
        stroke="white"
        fill="url(#bgMountainGradient1)"
        strokeWidth="0.6"
        d={svgPath}
        variants={mountainVariants}
        initial="initialDown"
        animate={mountainControl}
      />
    </motion.svg>
  );
}
