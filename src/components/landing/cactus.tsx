import { ColorMode } from "@chakra-ui/react";
import { motion, useAnimationControls, Variants } from "framer-motion";
import { useEffect } from "react";

interface Props {
  colorMode: ColorMode;
}

const cactusVariants: Variants = {
  initial: {
    opacity: 0,
    transition: {
      duration: 3.5,
    },
  },
  visible: {
    opacity: 1,
    transition: {
      delay: 1.5,
      duration: 3.5,
    },
  },
  initialScale: {
    transform: "scale(0, 0)",
  },
  scaleUp: {
    transform: "scale(-0.5, 0.5)",
    transition: {
      delay: 4,
      duration: 2.5,
    },
  },
};

function Cactus(props: Props) {
  const cactusCtrl = useAnimationControls();
  const { colorMode } = props;

  const toggleShadow = () => {
    if (colorMode == "light") {
      cactusCtrl.start("visible");
    } else {
      cactusCtrl.start("initial");
    }
  };

  useEffect(() => {
    toggleShadow();
  }, [colorMode]);

  useEffect(() => {
    toggleShadow();
  }, [colorMode]);

  return (
    <motion.div
      className="cactus"
      variants={cactusVariants}
      initial="initialScale"
      animate="scaleUp"
    >
      <motion.div
        variants={cactusVariants}
        initial="initial"
        animate={cactusCtrl}
        className="shadow"
      ></motion.div>

      <motion.div>
        <div className="right">
          <div className="cube top"></div>
          <div className="cube front"></div>
          <div className="cube side"></div>
        </div>

        <div className="fork">
          <div className="cube top"></div>
          <div className="cube front"></div>
        </div>
      </motion.div>
      <div className="middle">
        <div className="cube top"></div>
        <div className="cube front"></div>
        <div className="cube side"></div>
      </div>

      <motion.div>
        <div className="fork-left">
          <div className="cube top"></div>
          <div className="cube front"></div>
        </div>
        <div className="left">
          <div className="cube top"></div>
          <div className="cube front"></div>
          <div className="cube side"></div>
        </div>
      </motion.div>
    </motion.div>
  );
}

export default Cactus;
