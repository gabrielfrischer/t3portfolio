import { useEffect, useState, Dispatch, SetStateAction } from "react";
import { useColorMode, useMediaQuery } from "@chakra-ui/react";
import {
  motion,
  useAnimationControls,
  useAnimation,
  AnimationControls,
  Variants,
} from "framer-motion";
export interface Props {
  skyControl: AnimationControls;
  skyVariants: Variants;
  useDelay: number;
  setUseDelay: Dispatch<SetStateAction<number>>;
}

export default function SunMoon(props: Props) {
  const { skyControl, skyVariants, useDelay, setUseDelay } = props;

  const { colorMode, toggleColorMode } = useColorMode();
  const sunControl = useAnimationControls();
  const moonControl = useAnimationControls();

  const [isLargerThan1280] = useMediaQuery("(min-width: 480px)");

  const dynamicSunVariants = (useDelay: number) => {
    const sunVariants: Variants = {
      initialDraw: {
        pathLength: 0,
        opacity: 0,
        x: -50,
        y: 400,
        transition: {
          duration: 0.1,
        },
      },
      sunrise: {
        pathLength: 1,
        strokeWidth: 18,
        opacity: 1,
        strokeOpacity:0.3,
        x: 0,
        y: "10%",
        transition: {
        strokeWidth: {
          delay: 2,
          duration: 3.5,

          repeat: Infinity,
          repeatType: "mirror",
          ease: "easeInOut",
        },
        strokeOpacity: {
          delay: 2,
          duration: 3.5,

          repeat: Infinity,
          repeatType: "mirror",
          ease: "easeInOut",
        },
          duration: 2,
          delay: useDelay,
        },
      },
      sunset: {
        x: 40,
        y: 450,
        opacity: 0,
        transition: {
          duration: 1.5,
          delay: 0.1,
        },
      },
    };

    return sunVariants;
  };

  const moonVariants: Variants = {
    initialDraw: {
      opacity: 0,
      x: -20,
      y: 200,
      transition: {
        duration: 0.1,
      },
    },
    moonrise: {
      rotate: -20,
      opacity: 1,
      x: 0,
      y: -20,
      transition: {
        duration: 2,
        delay: 0.1,
      },
    },
    moonset: {
      opacity: 0,
      x: 50,
      y: 450,
      transition: {
        duration: 1.5,
        delay: 0.1,
      },
    },
  };

  const sunsetMoonriseSequence = async () => {
    setUseDelay(0);
    await sunControl.start("sunset");
    await sunControl.start("initialDraw");
    await moonControl.start("moonrise");
  };

  const sunriseMoonsetSequence = async () => {
    setUseDelay(0);
    await moonControl.start("moonset");
    await moonControl.start("initialDraw");
    await sunControl.start("sunrise");
  };

  const handleSunClick = () => {
    toggleColorMode();
    skyControl.start("dark");
    sunsetMoonriseSequence();
  };

  const handleMoonClick = () => {
    toggleColorMode();
    skyControl.start("light");
    sunriseMoonsetSequence();
  };

  useEffect(() => {
    setUseDelay(8);
    sunControl.start("sunrise");
  }, []);

  return (
    <motion.svg
      id="sunMoonContainer"
      className="svgContentSun parallax"
      version="1.1"
      width="200px"
      height="60%"
      preserveAspectRatio="xMidYMin meet"
      viewBox="0 0 200 100"
    >
      <motion.path
        stroke="white"
        onClick={handleSunClick}
        fill="url(#gradientSun)"
        className="svgSun pointer"
        strokeWidth="8"
        whileTap={{ scale: 0.95 }}
        strokeOpacity="0.6"
        variants={dynamicSunVariants(useDelay)}
        initial="initialDraw"
        animate={sunControl}
        d=" M  80   50  A  30 30 0 1 1 79.98172481057287 48.90 l 0 2"
      />
      <motion.path
        fill="url(#gradientMoon)"
        className="svgMoon pointer"
        strokeWidth="2"
        stroke="white"
        whileTap={{ scale: 0.95 }}
        strokeOpacity="0.6"
        //        A xR yR r s d xE yE
        d="M65 65 A 25 25 0 1 0 75 95 m 0 0 A 17 17 0 1 1 65 65"
        variants={moonVariants}
        initial="initialDraw"
        animate={moonControl}
        onClick={handleMoonClick}
      />

      <defs>
        <motion.radialGradient
          id="gradientSun"
          fr={0.2}
          fx={0.32}
          fy={0.32}
          r={0.7}
          animate={{ fr: 0.05, fx: 0.2, fy: 0.35, r: 0.6 }}
          transition={{
            repeat: Infinity,
            repeatType: "mirror",
            ease: "easeInOut",
            duration: 3,
          }}
        >
          <stop offset="0%" stopColor="#ff0" />
          <stop offset="100%" stopColor="#c50" />
        </motion.radialGradient>
        <motion.radialGradient
          id="gradientMoon"
          fr={0.2}
          fx={0.32}
          fy={0.32}
          r={0.7}
          animate={{ fr: 0.05, fx: 0.2, fy: 0.35, r: 0.6 }}
          transition={{
            repeat: Infinity,
            repeatType: "mirror",
            ease: "easeInOut",
            duration: 3,
          }}
        >
          <stop offset="0%" stopColor="#fff" />
          <stop offset="100%" stopColor="#d3d3d3" />
        </motion.radialGradient>
      </defs>
    </motion.svg>
  );
}
