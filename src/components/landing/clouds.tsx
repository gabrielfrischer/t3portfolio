import { motion, MotionStyle, Variants } from "framer-motion";
import React from "react";

interface Props {
  style?: MotionStyle;
  transformY?: number;
  transformX?: number;
  scrollY: number;
  offset: number;
}

function Clouds(props: Props) {
  const { offset, scrollY, style, transformX, transformY } = props;

  const cloudVariants: Variants = {
    initialDown: {
      x: 200,
      pathLength: 0,
    },
    animateUp: {
      x: 0,

      transition: {
        duration: 5,
        delay: 2.2,
      },
    },
    animatePath: {
      pathLength: 1,
      transition: {
        duration: 3.5,
      },
    },
  };

  return (
    <motion.svg
      width="100%"
      height="300px"
      preserveAspectRatio="xMaxYMin meet"
      viewBox="0 0 50 80"
      style={{
        position: "absolute",
        display: "block",
        zIndex: 997,
        pointerEvents: "none",
      }}
      animate={{
        y: `${offset * (scrollY * 0.3)}px`,
      }}
    >
      <motion.g
        variants={cloudVariants}
        initial="initialDown"
        animate="animateUp"
      >
        <defs>
          <filter id="filter" x="0" y="0">
            <feGaussianBlur stdDeviation=".4" />
          </filter>
        </defs>
        <motion.path
          fill="#fff"
          opacity={0.9}
          d="M 10 24 L 31 24 A 1 1 0 0 0 33 15 A 1 1 0 0 0 23 14 A 1 1 0 0 0 15 17 A 1 1 0 0 0 10 24"
          animate={{ x: -3, y: -3 }}
          transition={{
            repeat: Infinity,
            repeatType: "reverse",
            ease: "easeInOut",
            duration: 6,
          }}
          style={{
            ...style,
          }}
        />

        <motion.path
          fill="#fff"
          d="M 10 24 L 31 24 A 1 1 0 0 0 32 16 A 1 1 0 0 0 22 15 A 1 1 0 0 0 14 17 A 1 1 0 0 0 10 24"
          animate={{ x: -3, y: -3 }}
          transition={{
            repeat: Infinity,
            repeatType: "reverse",
            ease: "easeInOut",
            duration: 6,
          }}
          style={{
            ...style,
          }}
        />
      </motion.g>
      <defs>
        <motion.radialGradient
          id="gradientmoon"
          fr={0.2}
          fx={0.32}
          fy={0.32}
          r={0.7}
          animate={{ fr: 0.05, fx: 0.2, fy: 0.35, r: 0.6 }}
          transition={{
            repeat: Infinity,
            repeatType: "mirror",
            ease: "easeInOut",
            duration: 3,
          }}
        >
          <stop offset="0%" stopColor="#fff" />
          <stop offset="100%" stopColor="#d3d3d3" />
        </motion.radialGradient>
      </defs>
    </motion.svg>
  );
}

export default Clouds;
