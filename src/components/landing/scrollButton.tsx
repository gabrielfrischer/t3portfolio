import { Box, IconButton, useMediaQuery } from "@chakra-ui/react";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { motion, useAnimationControls, Variants } from "framer-motion";
import { useEffect } from "react";

interface Props {}

const ScrollButton = (props: Props) => {
  const [isLargerThan1280] = useMediaQuery("(min-width: 480px)");
  const scrollButtonCtrl = useAnimationControls();
  const scrollButtonVariants: Variants = {
    initial: {
      y: 5,
      opacity: 0,
    },
    animate: {
      y: -5,
      opacity: 1,
      transition: {
        opacity: {
          delay: 5,
        },
        y: {
          repeat: Infinity,
          repeatType: "reverse",
          ease: "easeInOut",
          duration: 2,
        },
      },
    },
  };

  useEffect(() => {
    scrollButtonCtrl.start("animate");
  }, []);

  function handleClick() {
    console.log("Clicked");
    const aboutMe = document.querySelector("#aboutme");
    if (aboutMe)
      aboutMe.scrollIntoView({
        behavior: "smooth",
      });
  }
  return (
    <motion.div
      style={{
        zIndex: 997,
      }}
      variants={scrollButtonVariants}
      initial="initial"
      animate={scrollButtonCtrl}
      whileHover={{ scale: 0.8 }}
    >
      <IconButton
        size="lg"
        onClick={handleClick}
        aria-label="Scroll to next section"
        borderRadius="25px"
        boxShadow=" 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
        icon={<ChevronDownIcon w={8} h={8} color="green.200" />}
      />
    </motion.div>
  );
};

export default ScrollButton;
