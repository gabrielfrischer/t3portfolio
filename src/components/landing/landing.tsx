import {
  CSSProperties,
  MouseEventHandler,
  useContext,
  useEffect,
  useRef,
  useState,
  MouseEvent,
} from "react";
import {
  useMediaQuery,
  useColorMode,
  Center,
  Box,
  Button,
  VStack,
} from "@chakra-ui/react";
import {
  useScroll,
  Variants,
  useAnimationControls,
  MotionStyle,
  ForwardRefComponent,
  HTMLMotionProps,
  useMotionValue,
  useTransform,
  motion,
  useInView,
} from "framer-motion";

import SunMoon from "./sunmoon";
import BgMountain from "./bgmountain";
import Clouds from "./clouds";
import Water from "./water";
import Bird from "./bird";
import TextLanding from "./textlanding";
import Cactus from "./cactus";
import Stars from "./stars";
import { NameContext } from "../../contexts/nameContext";
import ScrollButton from "./scrollButton";

const svgContainerStyle: CSSProperties = {
  width: "100%",
  height: "100%",
  overflowY: "auto",
};

const skyVariants: Variants = {
  light: {
    background:
      "linear-gradient(180deg, rgba(0,178,255,1) 0%, rgba(0,255,226,1) 33%, rgba(225,248,221,1) 61%)",

    transition: {
      duration: 4,
    },
  },
  dark: {
    background:
      "linear-gradient(180deg, rgba(84,0,163,1) 6%, rgba(0,61,170,1) 49%, rgba(89,99,86,1) 77%)",

    transition: {
      duration: 4,
    },
  },
};

const Landing = () => {
  const [useDelay, setUseDelay] = useState(2);
  const { colorMode, toggleColorMode } = useColorMode();

  const skyControl = useAnimationControls();
  const [scrollYOffset, setscrollYOffset] = useState(0);
  const landingRef = useRef<HTMLDivElement>(null);
  const { scrollY } = useScroll();

  const nameContext = useContext(NameContext);

  const ref = useRef(null);
  const isInView = useInView(ref, { once: false });

  useEffect(() => {
    //Ensure colormode starts as light for initial sun
    if (colorMode == "dark") {
      toggleColorMode();
    }

    //Begin Sky and Sun Animation
    skyControl.start("light");
    return scrollY.onChange((latest) => {
      setscrollYOffset(latest);
    });
  }, []);

  const scrollYValue = scrollY.get();

  const x = useMotionValue(200);
  const y = useMotionValue(200);

  const rotateX = useTransform(y, [0, 200], [15, -15]);
  const rotateY = useTransform(x, [0, 200], [-15, 15]);

  function handleMouse(event: MouseEvent<HTMLDivElement>) {
    const rect = event.currentTarget.getBoundingClientRect();

    x.set(event.clientX - rect.left);
    y.set(event.clientY - rect.top);
  }

  return (
    <main
      className=" mx-auto flex flex-col items-center justify-center min-h-screen"
      ref={ref}
      id="home"
    >
      <motion.div
        style={{
          width: "100%",
          height: "100vh",
          position: "relative",
          overflowY: "auto",
        }}
        variants={skyVariants}
        initial="dark"
        animate={skyControl}
        onMouseMove={handleMouse}
      >
        <div style={svgContainerStyle}>
          <SunMoon
            skyControl={skyControl}
            skyVariants={skyVariants}
            useDelay={useDelay}
            setUseDelay={setUseDelay}
          />
          <Stars colorMode={colorMode} />
          <Clouds
            offset={1.5}
            scrollY={isInView ? scrollYValue : 0}
            transformY={0}
            transformX={0}
          />
          <Bird
            offset={1.5}
            scrollY={isInView ? scrollYValue : 0}
            transformY={0}
            transformX={0}
            delay={1.6}
          />
          <Bird
            offset={1.5}
            scrollY={isInView ? scrollYValue : 0}
            transformY={10}
            transformX={20}
            delay={0.8}
          />
          <Center
            style={{
              width: "100%",
              height: "40%",
            }}
          >
            <VStack>
              <TextLanding
                offset={1.5}
                scrollY={isInView ? scrollYValue : 0}
                text={`Welcome ${nameContext?.name}`}
              />

              <ScrollButton />
            </VStack>
          </Center>
          {/* bg mountains */}
          <BgMountain
            colorMode={colorMode}
            offset={0.1}
            scrollY={isInView ? scrollYValue : 0}
            delay={0}
            style={{
              // transform: `translateY(calc(${scrollY}, 0) * 0.96))`,

              position: "absolute",
              top: 0,
              zIndex: 995,
            }}
            rotateX={rotateX}
          />
          <BgMountain
            colorMode={colorMode}
            offset={0.3}
            scrollY={isInView ? scrollYValue : 0}
            transformY={15}
            transformX={0}
            reverse={true}
            delay={0.2}
            style={{
              position: "absolute",
              top: 0,
              zIndex: 996,
              // transform: `translateY(calc(${scrollY}, 0) * 0.92))`,
            }}
            rotateX={rotateX}
          />
          <Cactus colorMode={colorMode} />
          <Center>
            <Water offset={0.9} transformY={0} transformX={0} />
          </Center>{" "}
          <Center></Center>
        </div>
      </motion.div>
    </main>
  );
};

export default Landing;
