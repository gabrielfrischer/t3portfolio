import { motion, MotionStyle, Variants } from "framer-motion";
import React from "react";

interface Props {
  style?: MotionStyle;
  transformY?: number;
  transformX?: number;
  scrollY: number;
  offset: number;
  delay?: number;
}

function Bird(props: Props) {
  const { offset, scrollY, style, transformX, transformY, delay } = props;

  const birdMoveVariants: Variants = {
    initialRight: {
      x: 300,
      scale: 0,
      pathLength: 0,
    },
    animateLeft: {
      x: "50%",
      y: 80,
      scale: 1,
      transition: {
        x: {
          duration: 4,
          delay: 2.2,
        },
        scale: {
          duration: 10,
        },
      },
    },
    animatePath: {
      pathLength: 1,
      transition: {
        delay: 2,
        duration: 3.5,
      },
    },
  };

  const birdWaveVariants: Variants = {
    initial: {
      pathLength: 0,
      scale: 0,
      d: "M 17 15 C 14 6 8 8 8 15 C 7 11 13 3 17 15 L 17 15 L 17 15 C 19 7 25 5 26 15 C 25 10 22 3 17 15",
    },
    wave: {
      scale: 1,
      d: "M 17 14 C 14 6 8 8 14 14 C 7 9 14 4 17 14 L 17 14 L 17 14 C 19 7 24 4 21 14 C 24 10 22 3 17 13",
      pathLength: 1,
      transition: {
        pathLength: {
          delay: 3,
          duration: 4,
          type: "tween",
        },
        scale: {
          duration: 10,
        },
        d: {
          duration: 2,

          delay: delay,
          repeat: Infinity,
          repeatType: "reverse",
          ease: "easeInOut",
        },
      },
    },
  };

  return (
    <motion.svg
      width="100%"
      height="200px"
      preserveAspectRatio="xMaxYMin meet"
      viewBox={` ${transformX}  ${transformY} 160 140`}
      style={{
        position: "absolute",
        display: "block",
        zIndex: 998,
        pointerEvents: "none",
        ...style,
      }}
      animate={{
        y: `${offset * (scrollY * 0.3)}px`,
      }}
    >
      <motion.g
        variants={birdMoveVariants}
        initial="initialRight"
        animate="animateLeft"
      >
        <defs>
          <filter id="filter" x="0" y="0">
            <feGaussianBlur stdDeviation=".4" />
          </filter>
        </defs>
        <motion.path
          stroke="black"
          scale={0.2}
          strokeWidth={1}
          fillOpacity={0.5}
          opacity={0.9}
          d="M 17 15 C 14 6 8 8 8 15 C 7 11 13 3 17 15 L 17 15 L 17 15 C 19 7 25 5 26 15 C 25 10 22 3 17 15"
          variants={birdWaveVariants}
          animate="wave"
          transition={{
            repeat: Infinity,
            repeatType: "reverse",
            ease: "easeInOut",
            duration: 6,
          }}
        />
      </motion.g>
      <defs>
        <motion.radialGradient
          id="gradientmoon"
          fr={0.2}
          fx={0.32}
          fy={0.32}
          r={0.7}
          animate={{ fr: 0.05, fx: 0.2, fy: 0.35, r: 0.6 }}
          transition={{
            repeat: Infinity,
            repeatType: "mirror",
            ease: "easeInOut",
            duration: 3,
          }}
        >
          <stop offset="0%" stopColor="#fff" />
          <stop offset="100%" stopColor="#d3d3d3" />
        </motion.radialGradient>
      </defs>
    </motion.svg>
  );
}

export default Bird;
