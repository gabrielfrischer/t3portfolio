import {
  Link as ChakraLink,
  Flex,
  useColorModeValue,
  FlexProps,
  Icon,
} from "@chakra-ui/react";
import Link from "next/link";
import { IconType } from "react-icons";
import {
  FiHome,
  FiTrendingUp,
  FiCompass,
  FiStar,
  FiMessageCircle,
} from "react-icons/fi";

interface LinkItemProps {
  name: string;
  icon: IconType;
}
const LinkItems: Array<LinkItemProps> = [
  { name: "Home", icon: FiHome },
  { name: "Trending", icon: FiTrendingUp },
  { name: "Explore", icon: FiCompass },
  { name: "Favourites", icon: FiStar },
  { name: "Contact", icon: FiMessageCircle },
];

interface NavItemProps extends FlexProps {
  icon: IconType;
  children: string;
}
const NavLink = ({ icon, children }: NavItemProps) => (
  <Link href={children == "Home" ? "/" : children.toLowerCase()} passHref>
    <ChakraLink style={{ textDecoration: "none" }}>
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: "cyan.400",
          color: "white",
        }}
      >
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: "white",
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </ChakraLink>
  </Link>
);

const NavLinks = () => {
  return (
    <>
      {LinkItems.map((link) => (
        <NavLink key={link.name} icon={link.icon}>
          {link.name}
        </NavLink>
      ))}
    </>
  );
};

export default NavLinks;
