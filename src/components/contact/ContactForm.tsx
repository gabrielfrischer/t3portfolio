import {
  Formik,
  FormikHelpers,
  FormikProps,
  Form,
  Field,
  FieldProps,
  FormikValues,
} from "formik";

import {
  Input,
  Button,
  Textarea,
  Center,
  VStack,
  IconButton,
  ColorMode,
} from "@chakra-ui/react";

import * as Yup from "yup";

import * as ga from "../../lib/ga";
import { FC, SetStateAction, useContext, useRef, useState } from "react";
import axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";
import { CustomInput } from "./CustomField";
import {
  EMAILJS_PUBLIC_KEY,
  EMAILJS_SERVICE_ID,
  EMAILJS_TEMPLATE_ID,
  RECAPTCHA_SITE_KEY,
} from "../../utils/constants";
import emailjs from "@emailjs/browser";
import { trpc } from "../../utils/trpc";
import { NameContext } from "../../contexts/nameContext";
import { AnimatePresence, motion } from "framer-motion";
import SubmitButton from "./SubmitButton";
import { ArrowForwardIcon, CheckCircleIcon } from "@chakra-ui/icons";
import { BiMailSend } from "react-icons/bi";

interface FormValues {
  name: string;
  email: string;
  subject: string;
  message: string;
  cc?: string;
}

interface Props {
  colorMode: ColorMode;
}

const contactSchema = Yup.object().shape({
  name: Yup.string()
    .min(1, "Too Short")
    .max(50, "Too Long")
    .required("Name required"),
  email: Yup.string()
    .email("Email must be of valid format")
    .required("Email Required"),
  subject: Yup.string()
    .min(1, "Too Short")
    .max(50, "Too Long")
    .required("Subject required"),
  message: Yup.string()
    .min(10, "Too Short")
    .max(1000, "Too Long")
    .required("Message is required"),
  cc: Yup.string().email("Must be email").optional(),
});

type Contact = Yup.InferType<typeof contactSchema>;

const ContactForm = (props: Props) => {
  const { colorMode } = props;
  const [token, setToken] = useState<SetStateAction<string | null>>(null);
  const [captchaResult, setCaptchaResult] = useState(null);
  const [emailSuccess, setEmailSuccess] = useState(false);
  const form = useRef<null | HTMLFormElement>(null);

  const nameContext = useContext(NameContext);
  const handleRecaptcha = async (token: string | null) => {
    if (!token) return;
    console.log("Token being sent to backend: ", token);
    setToken(token);
    const res = await axios.post(`/api/recaptcha/${token}`);
    console.log("Captcha Response: ", res);
    setCaptchaResult(res.data);
  };

  const getName = () => {
    let name = "";
    if (typeof nameContext?.name !== "string") {
      name = "";
    } else {
      name = nameContext?.name;
    }

    return name;
  };

  const initialValues: FormValues = {
    name: getName(),
    email: "",
    subject: "",
    message: "",
    cc: "",
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        setSubmitting(true);
        if (form.current == null) return;

        console.log("Values: ", values);
        if (typeof token !== "string") return;
        console.log("Form.current", form.current);
        console.log("EmailJS_serviceid: ", EMAILJS_SERVICE_ID);
        console.log("EmailJS_templateID: ", EMAILJS_TEMPLATE_ID);
        console.log("EmailJS_templateID: ", EMAILJS_PUBLIC_KEY);
        if (!form.current) return;
        if (typeof EMAILJS_SERVICE_ID !== "string") return;
        if (typeof EMAILJS_TEMPLATE_ID !== "string") return;

        emailjs
          .sendForm(
            EMAILJS_SERVICE_ID,
            EMAILJS_TEMPLATE_ID,
            form.current,
            EMAILJS_PUBLIC_KEY
          )
          .then(
            (response) => {
              console.log(response);
              if (response.status === 200) {
                console.log("Starting timer after 200");
                setTimeout(() => null, 3000);
                console.log("Timer ended");
                setEmailSuccess(true);
                setSubmitting(false);
              } else {
                console.log("Unexpected status code returned my EmailJS.");
              }
            },
            (error) => {
              console.log("Error in sending form.");
              console.log(error);
            }
          );
      }}
      validationSchema={contactSchema}
      validateOnMount
    >
      {({
        isSubmitting,
        isValid,
        handleSubmit,
        resetForm,
        values,
        validateForm,
      }) => (
        <Form
          ref={form}
          onSubmit={handleSubmit}
          id="contactForm"
          className="grid grid-cols-6"
        >
          <div className="col-span-6 md:col-start-1 md:col-span-2 lg:col-start-2 lg:col-span-2 ">
            <Field
              colorMode={colorMode}
              label="Name"
              name="name"
              type="input"
              as={CustomInput}
            />
            <Field
              colorMode={colorMode}
              label="Email"
              name="email"
              type="input"
              as={CustomInput}
            />
            <Field
              colorMode={colorMode}
              label="Subject"
              name="subject"
              type="input"
              as={CustomInput}
            />

            <Field
              colorMode={colorMode}
              label="Message"
              name="message"
              type="input"
              as={CustomInput}
              ct={true}
            />

            <Field
              colorMode={colorMode}
              label="CC"
              name="cc"
              type="input"
              as={CustomInput}
            />
            <motion.div
              initial={{
                opacity: 0,
                x: -200,
              }}
              animate={{
                opacity: isValid ? 1 : 0,
                x: isValid ? 0 : -200,
              }}
            >
              <ReCAPTCHA
                sitekey={`${RECAPTCHA_SITE_KEY}`}
                onChange={handleRecaptcha}
              />
            </motion.div>
          </div>
          <div className="col-span-6 md:col-span-3 lg:col-span-3 ">
            <Center className="md:h-full lg:h-full">
              <VStack>
                <AnimatePresence>
                  {captchaResult ? (
                    <motion.div
                      initial={{
                        scale: 0,
                        y: -200,
                      }}
                      animate={{
                        scale: isValid ? 1 : 0,
                        y: isValid ? 0 : -200,
                      }}
                    >
                      <>
                        {console.log("isSubmitting: ", isSubmitting)}
                        <Button
                          m={5}
                          aria-label={"Submit Button"}
                          h={100}
                          w={200}
                          rightIcon={
                            emailSuccess ? (
                              <CheckCircleIcon w={10} />
                            ) : (
                              <ArrowForwardIcon w={10} />
                            )
                          }
                          fontSize={27}
                          form="contactForm"
                          colorScheme="blue"
                          disabled={isSubmitting || emailSuccess}
                          isLoading={isSubmitting}
                          type="submit"
                        >
                          {emailSuccess ? "Sent!" : "Send Now"}
                        </Button>
                        {emailSuccess && (
                          <Button
                            onClick={() => {
                              resetForm({ values: initialValues });
                              setEmailSuccess(false);
                              setCaptchaResult(null);
                              validateForm();
                              window.grecaptcha.reset();
                            }}
                          >
                            Reset
                          </Button>
                        )}
                      </>
                    </motion.div>
                  ) : null}
                </AnimatePresence>

                <>
                  <motion.h1
                    initial={{
                      y: 0,
                      opacity: 1,
                      scale: 1,
                    }}
                    animate={{
                      y: isValid && captchaResult ? 200 : 0,
                      opacity: isValid && captchaResult ? 0 : 1,
                      scale: isValid && captchaResult ? 0 : 1,
                    }}
                    className="interFont text-center text-6xl  leading-normal font-bold  text-gray-700"
                  >
                    Contact <span className="text-slate-400">me</span>
                    <motion.p
                      style={{
                        fontSize: 16,
                      }}
                    >
                      I am open to new opportunities.
                    </motion.p>
                  </motion.h1>
                </>
              </VStack>
            </Center>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default ContactForm;
