import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  FormHelperText,
  ComponentWithAs,
  TextareaProps,
  Textarea,
  ColorMode,
} from "@chakra-ui/react";
import { FieldHookConfig, useField } from "formik";
import { FC } from "react";

// intersecting
interface ICustomFieldProps {
  label: string;
  ct?: boolean;
  textType: "textarea" | "input";

  colorMode: ColorMode;
}

const elementMap = { textarea: Textarea, input: Input };

export const CustomInput: FC<FieldHookConfig<string> & ICustomFieldProps> = ({
  label,
  ct,
  ...props
}) => {
  const { colorMode } = props;
  const [field, meta] = useField(props);

  return (
    <FormControl mb={1} isInvalid={meta.touched && !!meta.error}>
      <FormLabel>{label}</FormLabel>
      {}
      {ct ? (
        <Textarea
          bg={colorMode == "dark" ? "gray.700" : "gray.50"}
          {...field}
        />
      ) : (
        <Input bg={colorMode == "dark" ? "gray.700" : "gray.50"} {...field} />
      )}
      <FormHelperText color={colorMode == "dark" ? "gray.50" : "gray.700"}>
        {meta.error}
      </FormHelperText>
    </FormControl>
  );
};
