import React, { JSXElementConstructor, ReactElement, ReactNode } from "react";
import {
  useMediaQuery,
  Box,
  Text,
  Link,
  Center,
  VStack,
  useColorMode,
  Button,
} from "@chakra-ui/react";
import { motion, useInView } from "framer-motion";
import { useEffect, useRef, useState } from "react";
import { trpc } from "../utils/trpc";
import { Octokit, App } from "octokit";

import { GH_TOKEN } from "../utils/constants";
import GH_Repos from "./gh_repos";
import { v4 as uuidv4 } from "uuid";
import ChakraBox from "./chakraBox";
import ChakraSpan from "./chakraSpan";
import HandPic from "/wave.png";
import Image from "next/image";

import InViewBox from "./about_me/inViewBox";
import TechIcon from "./about_me/techIcon";
import UseChakraCarousel from "./about_me/useChakraCarousel";
import TechIcons from "./about_me/TechIcons";
import ContactForm from "./contact/ContactForm";
import InViewDiv from "./about_me/inViewDiv";
import DownloadLink from "./about_me/DownloadLink";
import dynamic from "next/dynamic";
import EducationList from "./about_me/educationList";
const PDFViewer = dynamic(import("../components/about_me/pdfViewer"), {
  ssr: false,
});
export interface Repos {
  full_name: string;
  created_at: string | null;
  url: string;
  key: string;
}

const AboutMe = () => {
  const [isLargerThan1280] = useMediaQuery("(max-width:480px)");
  const { colorMode, toggleColorMode } = useColorMode();
  // const hello = trpc.useQuery(["example.hello", { text: "from tRPC" }]);
  const octokit = new Octokit({ auth: GH_TOKEN });
  const [repos, setRepos] = useState<Repos[]>([]);
  // Compare: https://docs.github.com/en/rest/reference/users#get-the-authenticated-user

  const techIconsRef = useRef(null);
  const techInView = useInView(techIconsRef, { once: false, amount: 0.4 });

  useEffect(() => {
    const getUsername = async () => {
      const { data } = await octokit.rest.users.getAuthenticated();
      const { login } = data;
      console.log("Hello, %s", login);
    };

    const getRepos = async () => {
      console.log("Get Repos Running");
      const req = await octokit.request("GET /user/repos", {});
      if (req.status !== 200) return null;
      const { data } = req;
      console.log("GH Data: ", data);
      const init_repos: Repos[] = [];
      data.forEach((repo) => {
        const repoObj: Repos = {
          full_name: repo.full_name,
          created_at: repo.created_at,
          url: repo.html_url,
          key: uuidv4(),
        };
        init_repos.push(repoObj);
      });

      console.log("Init Repos: ", init_repos);
      setRepos(init_repos);
    };

    getUsername();
    getRepos();
    return () => {
      // this now gets called when the component unmounts
    };
  }, []);

  return (
    <Box>
      <Box className="grid grid-cols-6 min-h-screen">
        <InViewBox
          delay={0}
          className=" col-span-6 md:col-span-3 lg:col-span-3 "
          fromL={true}
          c_h="h-48 md:h-96 lg:h-96 "
          id="aboutme"
        >
          <Text className=" interFont text-left text-6xl  leading-normal font-bold  text-slate-400 px-6">
            I&apos;m{" "}
            <Link
              className="w-fit"
              bgGradient="linear(to-r, green.300, cyan.400)"
              bgClip="text"
            >
              Gabriel{" "}
            </Link>
          </Text>{" "}
          <motion.div
            style={{
              marginLeft: "10px",
            }}
            animate={{
              rotate: [0, 20],
              transition: {
                repeat: Infinity,
                repeatType: "mirror",
                type: "spring",
              },
            }}
          >
            <Image width={80} height={80} alt="wave" src="/wave.png" />
          </motion.div>
        </InViewBox>

        <InViewBox
          delay={0}
          c_h="h-48 md:h-96 lg:h-96 "
          className="col-span-6 md:col-span-3 lg:col-span-3 flex flex-row  interFont text-4xl p-4  leading-normal font-extrabold text-gray-700 "
        >
          <Box>
            <ChakraSpan>I</ChakraSpan>
            <motion.div
              initial={{
                opacity: 0.4,
                scale: 1,
                y: 0,
              }}
              animate={{
                y: -10,
                scale: 1.27,
                opacity: 1,
                transition: {
                  repeat: Infinity,
                  duration: 1.4,
                  repeatType: "mirror",
                  ease: "easeInOut",
                },
              }}
              className="text-pink-400 inline-block"
            >
              ❤️
            </motion.div>
            <ChakraSpan>the </ChakraSpan>
            <ChakraSpan
              bgGradient="linear(to-r, green.300, cyan.400)"
              bgClip="text"
            >
              gym grind
            </ChakraSpan>
            ,
            <ChakraSpan
              bgGradient="linear(to-r, green.300, cyan.400)"
              bgClip="text"
            >
              {" "}
              learning about others
            </ChakraSpan>
            , and
            <ChakraSpan
              bgGradient="linear(to-r, green.300, cyan.400)"
              bgClip="text"
            >
              {" "}
              building great software
            </ChakraSpan>
            !
          </Box>
        </InViewBox>

        <InViewBox
          delay={0.5}
          c_h="h-auto "
          className="col-span-6 md:col-span-6 lg:col-span-6"
          fromL={true}
          id="mystack"
        >
          <Center>
            <VStack>
              <motion.h1 className="interFont text-center text-6xl  leading-normal font-bold  text-gray-700">
                I make
                <span className="text-slate-400"> modern web apps</span>
              </motion.h1>
              <TechIcons />
            </VStack>
          </Center>
        </InViewBox>
        <InViewDiv
          delay={0}
          className=" col-span-6 py-5 mt-5   "
          fromL={true}
          c_h="h-auto "
          id="Resume"
        >
          <VStack>
            <motion.h1 className="interFont text-center text-6xl  leading-normal font-bold  text-gray-700">
              My
              <span className="text-slate-400"> Education</span>
            </motion.h1>
            <EducationList />
            <DownloadLink text="Download Resume" path="/ConciseResume.pdf" />
            <PDFViewer />
          </VStack>
        </InViewDiv>
        <InViewBox
          delay={0.5}
          c_h="h-auto md:h-auto lg:h-auto mb-6"
          className="col-span-6  md:col-span-6  lg:col-span-6 flex flex-col  "
        >
          <div className="grid grid-cols-6 ">
            <div className="col-span-6 flex justify-center">
              <GH_Repos repos={repos} />
            </div>

            <div className="col-span-6 my-5">
              <motion.h1 className="interFont text-center text-4xl  leading-normal font-bold  text-gray-700">
                {repos.length}
                <span className="text-slate-400"> public repos</span> /{" "}
                {" " + 41}
                <span className="text-slate-400"> private repos</span>
              </motion.h1>
            </div>
          </div>
        </InViewBox>

        <InViewDiv
          id="portfolio"
          delay={1.5}
          c_h="h-auto md:h-auto lg:h-auto mb-6 p-8"
          className="col-span-6  md:col-span-6  lg:col-span-6 flex flex-col  text-gray-700 "
        >
          <motion.h1 className="interFont text-center text-6xl  leading-normal font-bold  text-gray-700">
            Projects <span className="text-slate-400">I love</span>
          </motion.h1>
          <div className="w-full">
            <UseChakraCarousel />
          </div>
        </InViewDiv>
      </Box>

      <Box className="">
        <InViewDiv c_h="p-5" fromL={true} id="contact" className="">
          <ContactForm colorMode={colorMode} />
        </InViewDiv>
      </Box>
      {/* 
      <div className="pt-6 text-2xl text-blue-500 flex justify-center items-center w-full">
        {hello.data ? <p>{hello.data.greeting}</p> : <p>Loading..</p>}
      </div> */}
    </Box>
  );
};

export default AboutMe;
