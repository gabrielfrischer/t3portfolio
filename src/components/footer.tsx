import {
  Link as ChakraLink,
  Box,
  chakra,
  Container,
  Link,
  Stack,
  Text,
  useColorModeValue,
  VisuallyHidden,
} from "@chakra-ui/react";
import {
  FaInstagram,
  FaTwitter,
  FaYoutube,
  FaGitlab,
  FaGithub,
  FaLinkedin,
} from "react-icons/fa";
import { ReactNode } from "react";
import Logo from "./svgs/logo";
import LinkItems from "./linkItems";
import { IconType } from "react-icons";
import NavLinks from "./scroll_links";

const SocialButton = ({
  children,
  label,
  href,
}: {
  children: ReactNode;
  label: string;
  href: string;
}) => {
  return (
    <chakra.button
      bg={useColorModeValue("blackAlpha.100", "whiteAlpha.100")}
      rounded={"full"}
      w={8}
      h={8}
      cursor={"pointer"}
      as={"a"}
      href={href}
      display={"inline-flex"}
      alignItems={"center"}
      justifyContent={"center"}
      transition={"background 0.3s ease"}
      _hover={{
        bg: useColorModeValue("blackAlpha.200", "whiteAlpha.200"),
      }}
    >
      <VisuallyHidden>{label}</VisuallyHidden>
      {children}
    </chakra.button>
  );
};

interface NavItemProps {
  children: string;
}

function FooterLink({ children }: NavItemProps) {
  function handleClick(f_name: string) {
    console.log("Clicked");
    console.log("fname: ", f_name);
    const section = document.querySelector("#" + f_name);
    console.log("section outside if:  ", section);
    if (section) {
      console.log("section exists: ", section);
      section.scrollIntoView({
        behavior: "smooth",
      });
    }
  }

  const f_name = children.toLowerCase().replace(/\s+/g, "");

  return (
    <ChakraLink
      onClick={() => handleClick(f_name)}
      style={{ textDecoration: "none" }}
    >
      {children}
    </ChakraLink>
  );
}

export default function Footer() {
  let year: Date | number = new Date();
  year = year.getFullYear();

  return (
    <Box
      bg={useColorModeValue("gray.100", "gray.900")}
      color={useColorModeValue("gray.700", "gray.200")}
    >
      <Container
        as={Stack}
        maxW={"6xl"}
        py={4}
        spacing={4}
        justify={"center"}
        align={"center"}
      >
        <Logo />

        <Stack direction={"row"} spacing={6}>
          {LinkItems.map((link) => (
            <FooterLink key={link.name}>{link.name}</FooterLink>
          ))}
        </Stack>
      </Container>

      <Box
        borderTopWidth={1}
        borderStyle={"solid"}
        borderColor={useColorModeValue("gray.200", "gray.700")}
      >
        <Container
          as={Stack}
          maxW={"6xl"}
          py={4}
          direction={{ base: "column", md: "row" }}
          spacing={4}
          justify={{ base: "center", md: "space-between" }}
          align={{ base: "center", md: "center" }}
        >
          <Text>{year} Gabriel Frischer. All rights reserved</Text>
          <Stack direction={"row"} spacing={6}>
            <SocialButton
              label={"Github"}
              href={"http://github.com/gabrielfrischer"}
            >
              <FaGithub />
            </SocialButton>
            <SocialButton
              label={"GitLab"}
              href={"https://gitlab.com/gabrielfrischer"}
            >
              <FaGitlab />
            </SocialButton>
            <SocialButton
              label={"LinkedIn"}
              href={"https://www.linkedin.com/in/gabrielfrischer/"}
            >
              <FaLinkedin />
            </SocialButton>
          </Stack>
        </Container>
      </Box>
    </Box>
  );
}
