import {
  Formik,
  Form,
  Field,
  FieldProps,
  FieldInputProps,
  FormikProps,
} from "formik";
import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  HStack,
} from "@chakra-ui/react";
import { motion, useAnimationControls, Variants } from "framer-motion";
import * as Yup from "yup";
import { Dispatch, SetStateAction } from "react";

type FormValues = {
  name: string;
};

interface Props {
  onClose: () => void;
  setName: Dispatch<SetStateAction<string | null>>;
}

const validationSchema = Yup.object({
  name: Yup.string(),
});

const isInvalidForm = (form: FormikProps<FormValues>): boolean => {
  return Boolean(form.errors.name && form.touched.name);
};

const StartForm = ({ onClose, setName }: Props) => {


const skipVariant: Variants = {
  initial: { opacity: 1 },
  isFilled: { opacity: 0 },
};


  return (
    <Formik
      initialValues={{ name: "" }}
      validationSchema={validationSchema}
      onSubmit={(values, actions) => {
        console.log("Values: ", values);
        console.log("Submitted");
        setName(values.name);
        onClose();
      }}
    >
      {(props) => (
        <Form className="w-full ">
          <HStack>
            <Field name="name" width="100%">
              {({
                field,
                form,
              }: {
                field: FieldInputProps<string>;
                form: FormikProps<FormValues>;
              }) => (
                <FormControl isInvalid={isInvalidForm(form)}>
                  <Input
                    {...field}
                    variant="filled"
                    bg="gray.100"
                    color="messenger"
                    placeholder="Enter name here"
                  />
                  <FormErrorMessage>{form.errors.name}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <Button
              mt={4}
              colorScheme="messenger"
              isLoading={props.isSubmitting}
              type="submit"
            >
Let&apos;s Go!
            </Button>
          </HStack>
          <motion.div variants={skipVariant} initial="initial" animate={props.values.name.length > 0 ? "isFilled" : "initial" } className="w-100 mt-2  flex items-center justify-center  ">
            <Button
              onClick={() => onClose()}
              size="xs"
              z-index="999"
              colorScheme="black"
              variant="outline"
            >
              Skip
            </Button>
          </motion.div>
        </Form>
      )}
    </Formik>
  );
};

export default StartForm;
