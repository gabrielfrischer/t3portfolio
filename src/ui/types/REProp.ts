import { ReactElement } from "react";

export type REType = {
    children: ReactElement | ReactElement[];
}