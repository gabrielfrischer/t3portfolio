import {
  useEffect,
  useState,
  useRef,
  MutableRefObject,
  ReactElement,
  RefObject,
  useLayoutEffect,
} from "react";
import Navbar from "../components/navbar";
import Footer from "../components/footer";
import { REType } from "./types/REProp";
import {
  Progress,
  VStack,
  useDisclosure,
  Center,
  Text,
  useColorModeValue,
  useColorMode,
  Button,
} from "@chakra-ui/react";
import { useScrollIndicator } from "react-use-scroll-indicator";
import StartForm from "../components/start_form";

import {
  AnimatePresence,
  useAnimationControls,
  Variant,
  Variants,
  motion,
  useScroll,
  useSpring,
  useMotionValue,
} from "framer-motion";
import { isDev } from "../utils/constants";
import ChakraBox from "../components/chakraBox";
import { NameInterface } from "../contexts/nameContext";
import { NameContext } from "../contexts/nameContext";

interface scrollProps {
  ref_prop: RefObject<HTMLDivElement>;
}
const ScrollProgress = (props: scrollProps) => {
  const { ref_prop } = props;

  const maxScrollValue = useMotionValue(1);

  const { scrollYProgress } = useScroll({
    target: ref_prop,
  });
  const yProgress = scrollYProgress == maxScrollValue ? 0 : scrollYProgress;

  const scaleX = useSpring(yProgress, {
    stiffness: 100,
    damping: 30,
    restDelta: 0.001,
  });
  return (
    <ChakraBox
      className="progressBar"
      bg={useColorModeValue("blue.400", "blue.100")}
      style={{ scaleX }}
    />
  );
};

const fadeVariants: Variants = {
  initial: { opacity: 0, y: 100 },
  fadeIn: { opacity: 1, y: 0, transition: { delay: 0.1, duration: 0.6 } },
  exit: { opacity: 0, transition: { delay: 0.1, duration: 0.6 } },
};
const slideVariants: Variants = {
  initial: { y: 100 },
  slideIn: { y: 0, transition: { duration: 0.6 } },
};

const homeVariants: Variants = {
  initial: { opacity: 0 },
  fadeIn: {
    opacity: 1,

    transition: { delay: 1, duration: 0.5 },
  },
};

const Layout = ({ children }: REType) => {
  const { isOpen, onClose } = useDisclosure({ defaultIsOpen: true });
  const nameBoxControls = useAnimationControls();
  const slideControls = useAnimationControls();
  const homeControls = useAnimationControls();

  const { colorMode, toggleColorMode } = useColorMode();

  const [displayHome, setDisplayHome] = useState(false);
  const [render, setRender] = useState(false);
  const [name, setName] = useState<null | string>("");

  const homePageRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    console.log("colormode: ", colorMode);
    //Ensure colormode starts as light for initial sun
    if (colorMode == "dark") {
      toggleColorMode();
    }

    const bodyScroll = document.querySelector("body");
    const htmlScroll = document.querySelector("html");
    // if (htmlScroll != null) htmlScroll.style.overflowY = "hidden";

    // if (bodyScroll != null) bodyScroll.style.overflowY = "hidden";

    nameBoxControls.start("fadeIn");
    slideControls.start("slideIn");
  }, []);

  useEffect(() => {
    homeControls.start("fadeIn");
    const htmlScroll = document.querySelector("html");
    if (htmlScroll != null) htmlScroll.style.overflowY = "overlay";
    delayRender();
  }, [displayHome]);

  const delayRender = () => {
    setTimeout(() => {
      setRender(true);
    }, 7000);
  };

  const handleOnClose = () => {
    onClose();

    setDisplayHome(true);
    nameBoxControls.start("exit");
  };

  return (
    <>
      <NameContext.Provider value={{ name: name }}>
        <AnimatePresence>
          {!displayHome && (
            <motion.div
              variants={fadeVariants}
              initial="initial"
              animate={nameBoxControls}
              exit={"exit"}
            >
              <Center minHeight="100vh" p={5} bg="white" color="black">
                <motion.div
                  variants={slideVariants}
                  initial="initial"
                  animate={slideControls}
                >
                  <VStack w="full" h="full" alignItems="center">
                    <Text fontSize="3xl">{"What can I call you? 😏"}</Text>
                    <StartForm onClose={handleOnClose} setName={setName} />
                  </VStack>
                </motion.div>
              </Center>
            </motion.div>
          )}
        </AnimatePresence>
        <AnimatePresence>
          {displayHome && (
            <motion.div
              id="motionDiv"
              initial="initial"
              variants={homeVariants}
              animate={homeControls}
              className="title has-text-weight-bold is-1 is-size-2-mobile is-spaced"
              layoutId="title"
              ref={homePageRef}
            >
              <Navbar>
                <motion.main>{children}</motion.main>
                <Footer />
                <AnimatePresence>
                  {render && <ScrollProgress ref_prop={homePageRef} />}
                </AnimatePresence>
              </Navbar>
            </motion.div>
          )}
        </AnimatePresence>
      </NameContext.Provider>
    </>
  );
};

export default Layout;
